''' 
    Data Rights Notice

        This software was produced for the U. S. Government under Basic Contract No. W15P7T-13-C-A802, and is subject to the Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation Clause 252.227-7014 (FEB 2012)    
        Copyright 2017 The MITRE Corporation. All Rights Reserved.


    :author: Kevin Farr - kfarr@mitre.org; Mike Kumaresan - nkumaresan@mitre.org
    :data: 01/21/2019


'''

import os
import requests
import json
import struct
import socket
import time
import multiprocessing

from src.network.sockets_manager import connect

from google.protobuf.json_format import MessageToJson

from src.logger.eventLogger import eventLogger, LOG_TYPE_CRITICAL, LOG_TYPE_ERROR, LOG_TYPE_INFO, LOG_TYPE_WARNING

# Logging TAG
TAG = '[ASI PROXY] '

class Tuner():
    '''
    This class contains the business logic for Tuning a radio

    '''

    def __init__(self, on_re_tune, config, logger):

        self.callback = on_re_tune

        # default parameters
        self.connection_status = "Not Connected"
        self.connected = False
        self.radio_connection_socket = None

        # load config
        self.config = config

        # logger
        self.logger = logger

        # radio control address and port
        self.radio_control_ip_address = None
        self.radio_control_ip_port = None

        # determine radio Control IP and Port
        self.radio_control_ip_address = config['asi_radio_control_ip']
        self.data_rcv_port = config['asi_rcv_port']
        self.ten_gbe_ip_2 = config['asi_dest_ip_two']
        self.ten_gbe_ip_1 = config['asi_dest_ip_one']
        self.radio_endpoint = config['asi_radio_endpoint']
        self.timeout = config['rest_timeout']
        self.print_resp = config['print_response']

        #Context Socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.active_stream = False
        
        #Start Context stream thread
        rate_secs = config['context_rate_sec']
        self.stream = multiprocessing.Process(target=self.periodic_context,args=(rate_secs,))
     

     

    '''
     #================================
     Actions
     #================================
    '''

    def send_context(self,payload,ports):
        for port in ports:
            self.sock.sendto(payload, ("172.17.0.1", port))


    def periodic_context(self,rate_sec=1):
        while True:
            verify_response, verify_info_dict = self.get(self.radio_control_ip_address, self.radio_endpoint, self.timeout) 
            #print('Verify Response : {}'.format(verify_response))
            #print('Verify Info Dict : {}'.format(verify_info_dict))
            try:
                # Verify response
                if verify_response != '<Response [200]>':
                
            
                    sample_rate=verify_info_dict[1]['sample_rate'][0]
                    frequency=verify_info_dict[1]['frequency'][0]
                    gain = verify_info_dict[1]['gain'][0]
            
                    message = struct.pack('ffi',sample_rate,frequency,gain)
                    self.send_context(payload=message, ports=self.data_rcv_port)
                time.sleep(rate_sec)
            except:
                time.sleep(1)
        

    def frq_cmd(self, center_freq, tuner_list, sample_rate=20480000, gain=76):
        '''
        REST PUT command for tuning radio
        :param tuners: Specific tuners
        :param center_freq: Center Frequency in Hz
        :param sample_rate: Sample rate in Samples/Second
        :param gain: Gain in dB
        :return: Response of PUT command
        '''

        ip_address_list = [self.ten_gbe_ip_1, self.ten_gbe_ip_2, self.ten_gbe_ip_1, self.ten_gbe_ip_2, self.ten_gbe_ip_1, self.ten_gbe_ip_2, self.ten_gbe_ip_1, self.ten_gbe_ip_2, self.ten_gbe_ip_1, self.ten_gbe_ip_2]

        active_list = []
        channel_list = []
        for tuner in range(len(tuner_list)):
            active_list.append(True)
            channel_list.append(tuner_list[tuner])


        payload = {'active': active_list, 'frequency': center_freq, "sample_rate": sample_rate, "gain": gain, "ip_addr": ip_address_list, "ip_port": self.data_rcv_port, "channels": channel_list}
        self.put(self.radio_control_ip_address, self.radio_endpoint, {'active': False}, timeout=self.timeout)
        return self.put(self.radio_control_ip_address, self.radio_endpoint, payload, timeout=self.timeout)

    def add_tuner(self, tuner_id_list, center_freq_hz):
        '''
        Creates a tuner request to the radio by applying a REST PUT command to the web interface
        :param tuner_id_list: tuners to apply changes to
        :param center_freq_hz: Center frequency in Hz
        :return: Boolean Tune Completion
        '''
        try:
            print('Tuning on channels: ' + str(tuner_id_list))

            # Sending context packets on
            self.send_context(struct.pack('ffi', 0.0, 0.0, 0), self.data_rcv_port)

            #Send PUT command to tune radio
            self.frq_cmd(center_freq=center_freq_hz, tuner_list=tuner_id_list)
            verify_response, verify_info_dict = self.get(self.radio_control_ip_address, self.radio_endpoint, self.timeout)

            if verify_info_dict is not None:
                sample_rate=verify_info_dict[1]['sample_rate'][0]
                frequency=verify_info_dict[1]['frequency'][0]
                gain = verify_info_dict[1]['gain'][0]
                if self.print_resp:
                    self.resp_print(verify_info_dict)

                if not self.print_resp:
                    print("Tuned to Frequency {}".format(frequency))
                    print("Sampling Rate {}".format(sample_rate))

                self.send_context(payload=struct.pack('ffi', sample_rate, frequency, gain), ports=self.data_rcv_port)

                return True
            else:
                print(TAG, 'Response Timed Out from StackSDR server')
                return None
        except KeyError as err:
            print('{}'.format(err))
            return False

    def connect_to_radio(self):
        '''
        Check if radio is connected via using a REST Get call to the radio web interface. If a response is given, then
        then radio connection is validated.
        :return: Boolean (Connected, Not Connected)
        '''

        print('Connecting to Radio...')
        self.logger.write(LOG_TYPE_INFO, "Attempting to connect and query Radio...")

        # ignore any proxy configuration for REST requests
        os.environ['no_proxy'] = self.radio_control_ip_address

        # setup network session with asi
        if self.radio_connection_socket==None:
            connect_response, connect_info_dict = self.get(self.radio_control_ip_address, self.radio_endpoint) 
            if str(connect_response)=='<Response [200]>':
                self.radio_connection_socket=True
                self.connected=True

            if self.radio_connection_socket is not None:
                self.logger.write(LOG_TYPE_INFO, "Succesfully connected to radio.")
                if self.print_resp:
                    self.resp_print(connect_info_dict)
                self.stream.start()
                return True
            else:
                print("Error - Failed to connect to radio.")
                self.connected = False
                self.logger.write(LOG_TYPE_ERROR, "Failed to connect to radio.")
                return False
        else:
            print("Error - Already connected to radio.")
            self.logger.write(LOG_TYPE_ERROR, "Already connected to ASI.")
            return False


    '''
        #================================
        REST API Calls
        #================================
    '''
    def get(self, radio_ip, endpoint, time_out=2):
        '''
        REST API GET call to query the asi web interface
        :param radio_ip: ip address of radio to access webpage
        :param endpoint: rest interface port number
        :param time_out: query timeout in seconds
        :return: response from the rest
        '''
        CONTROL_INTERFACE = "/api/v2/stream"
        req_string = "http://{}:{}".format(radio_ip,endpoint) + CONTROL_INTERFACE

        try:
            response = requests.get(req_string, timeout=time_out)
            info_dict = json.loads(response.content)
            return response, info_dict
        except Exception as e:
            print(e)
            return None, None

    def put(self, radio_ip, endpoint, payload, timeout=2):
        '''
        REST API PUT command to establish settings for tuning on web interface
        :param radio_ip: Web IP address
        :param endpoint: Web interface endpoint port
        :param payload: Message in JSON
        :param timeout: Timeout before error
        :return: Response and corresponding dictonary of message
        '''
        CONTROL_INTERFACE = "/api/v2/stream"
        if self.print_resp:
            print("")
            print('======Debug Mode:Message Context=====')
            print(payload)  
            print("")
        try:
            req_string = "http://{}:{}".format(radio_ip,endpoint) + CONTROL_INTERFACE
            response = requests.put(req_string, json=payload, timeout=timeout)
            info_dict = json.loads(response.content)
            return response, info_dict
        except Exception as e:
            print(e)
            return None, None

    def resp_print(self, dict):
        print("")
        print('======Debug Mode:Response Context=====')
        print('-->Data Source {}'.format(dict[1]['data_source']))
        print('-->Gain Mode   {}'.format(dict[1]['gain_mode']))
        print('-->Sample Rate {} MSps'.format([x/1e6 for x in dict[1]['sample_rate']]))
        print('-->Bandwidth   {} MHz'.format([x/1e6 for x in dict[1]['bandwidth']]))
        print('-->Frequency   {} MHz'.format([x/1e6 for x in dict[1]['frequency']]))
        print('-->Gain        {} dB'.format(dict[1]['gain']))
        print('-->ChannelMode {} '.format(dict[1]['channel_mode']))
        print("")




