''' 
    Data Rights Notice
        
        This software was produced for the U. S. Government under Basic Contract No. W15P7T-13-C-A802, and is subject to the Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation Clause 252.227-7014 (FEB 2012)    
        Copyright 2017 The MITRE Corporation. All Rights Reserved.
               
    .. note ::
        Classes is this module:
            :class:`eventLogger`
                
    :author: Bill Urrego - wurrego@mitre.org
    :data: 09/04/17            
                
'''
from src.license import DATA_RIGHTS
from src.version import __version__

__author__ = "Bill Urrego - wurrego@mitre.org"
__license__ = DATA_RIGHTS
__version__ = __version__
__last_modified__ = '9/04/17'

import logging
from logging.handlers import RotatingFileHandler

LOG_TYPE_ERROR = 'error'
LOG_TYPE_CRITICAL = 'critical'
LOG_TYPE_WARNING = 'warning'
LOG_TYPE_INFO= 'info'

class eventLogger():
    """This class contains the methods to log import events to file

            .. note::
                log files will be located in htl_proxy/logger/

            .. note::
                Constants used by the following methods:
                    * LOG_TYPE_ERROR = 'error'
                    * LOG_TYPE_CRITICAL = 'critical'
                    * LOG_TYPE_WARNING = 'warning'
                    * LOG_TYPE_INFO= 'info'


            .. note::
                Class methods:
                    * :func:`__init__`
                    * :func:`write`

         """

    def __init__(self):
        """This will setup the log file write directory, file size before rotation, and headers

        """
        # set log name
        self.name = 'htl_proxy' + __version__
        self.logger = logging.getLogger(self.name)

        # create file handler
        logFile = 'logger/' + self.name + '.log'
        self.max_logfile_size_bytes = 20 * 1024 * 1024
        self.handler = RotatingFileHandler(logFile, mode='a', maxBytes=self.max_logfile_size_bytes, backupCount=2, encoding=None, delay=0)

        # format log file
        self.formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)

        # Set level
        self.logger.setLevel(logging.INFO)

    def write(self, log_type, message):
        """This will write the message to the log file with the severity specified by log_type
                
                :param log_type: severity see notes
                :type: str
                :param message: the message to write to the log file
                :type: str
                
               .. note::
                   use the following constants for log_type:
                        * LOG_TYPE_ERROR = 'error'
                        * LOG_TYPE_CRITICAL = 'critical'
                        * LOG_TYPE_WARNING = 'warning'
                        * LOG_TYPE_INFO= 'info'

        """

        if log_type == LOG_TYPE_CRITICAL:
            self.logger.critical(message)

        elif log_type == LOG_TYPE_WARNING:
            self.logger.warning(message)

        elif log_type == LOG_TYPE_ERROR:
            self.logger.error(message)

        else:
            self.logger.info(message)
