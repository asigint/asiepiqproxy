''' 
    Data Rights Notice

        This software was produced for the U. S. Government under Basic Contract No. W15P7T-13-C-A802, and is subject to the Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation Clause 252.227-7014 (FEB 2012)    
        Copyright 2017 The MITRE Corporation. All Rights Reserved.


    :author: Bill Urrego - wurrego@mitre.org
    :data: 03/12/18


'''

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import socket
import threading
from threading import Thread
import time
import queue
import os
import json
import requests

import src.TEWSuServiceMessaging_pb2 as MicroMessage
from google.protobuf.json_format import MessageToJson




def connect(ip_address, port, timeout_seconds=5):
    '''

    :param ip_address:
    :param port:
    :param timeout_seconds:
    :return:
    '''

    connection = None

    try:
        # Open a TCP/IP socket to the HTL Device
        connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connection.settimeout(timeout_seconds)
        connection.connect((ip_address, port))
        

    except:
        connection = None

    return connection


class MonitorRadio(threading.Thread):
    ''' Uses REST API to retrieve status from a Herrick HTL, create an HTLStatus packet, and send it to tewspectrum
        This is a separate thread from ReceivePntInfo to accommodate different update rates.

    '''


    def __init__(self, config, radio_id):
        super(MonitorRadio, self).__init__()

        self.config = config
        self.radio_id = radio_id

        self.send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.send_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        self.shutdown_flag = threading.Event()

        self.radio_ip_addr = self.config['radio_ip_address']

        self.tews_dest_ip_addr = self.config['tews_proto_dest_ip']
        self.tews_dest_port = self.config['tews_proto_dest_port']


        # ignore any proxy configuration for REST requests
        os.environ['no_proxy'] = self.radio_ip_addr

        print("MonitorRadio: radio = " + self.radio_ip_addr + " sleep_time = " + str(self.sleep_time))

    def run(self):

        while not self.shutdown_flag.is_set():

            try:
                # make REST API call and convert response to dictionary
                info_dict = ConfigRestApi.get(self.radio_ip_addr, ConfigRestApi.CONTROL_INTERFACE, 0.2)

                # Check if radio is connected
                if not info_dict:

                    print("ASI Radio Connected")

                time.sleep(self.sleep_time)

            except Exception as e:
                print("MonitorRadio exception: " + str(e))
                time.sleep(self.sleep_time)


class ReceivePackets(threading.Thread):
    '''
        Sample thread for receiving packets from a radio where a TCP/IP connection has been created for control
        and monitoring.
    '''

    def __init__(self, connection_socket, buffer_size, callback_queue, config, parent):
        super(ReceivePackets, self).__init__()

        self.shutdown_flag = threading.Event()

        self.send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.send_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        self.connection_socket = connection_socket
        self.buffer_size = buffer_size

        self.callback_queue = callback_queue

        self.config = config

        self.parent = parent


    def run(self):

        while not self.shutdown_flag.is_set():
            try:
                # Receive the message data
                data = self.connection_socket.recv(self.buffer_size)

                idx = 0

                # A TCP socket can receive partial or multiple messages.  The application is responsible for
                # accurately receiving and constructing the message.  The following code shows receiving a
                # complete Google protobuf message before passing it to a parsing method
                while idx < len(data):


                    remaining_data_size = len(data) - idx

                    # First, make sure you've received at least a full header's worth of information.
                    # "RADIO_HEADER_SIZE" would be defined to be the length of the header information in
                    # the protocol being received
                    if remaining_data_size < RADIO_HEADER_SIZE:
                        continue

                    # Parse Header
                    # "RADIO_HEADER_MSG_LEN_SIZE" is the number of bytes encoding the message length
                    message_length = int.from_bytes(data[idx : idx + RADIO_HEADER_MSG_LEN_SIZE], byteorder='big')

                    # move index past header message length
                    idx += RADIO_HEADER_MSG_LEN_SIZE

                    # Next, our sample header has an ID to parse.
                    message_id = int.from_bytes(data[idx : idx + RADIO_HEADER_MSG_ID_SIZE], byteorder='big')

                    # move index past header message ID length
                    idx += RADIO_HEADER_MSG_ID_SIZE

                    # Done parsing header.  Calculate how much additional data (if any) has to be received.
                    remaining_data_size = len(data) - idx

                    # not enough data to parse message, break out of loop to receive some more
                    if remaining_data_size <= 0:
                        continue

                    # Assign the actual message data to msg_buf
                    msg_buf = data[idx : idx + (message_length - RADIO_HEADER_SIZE)]

                    # Pass msg_buff to a parse_message method
                    self.parse_message(message_id, msg_buf)

                    # move index past message
                    idx += (message_length - RADIO_HEADER_SIZE)

            except socket.timeout:
                # if the socket times out, that's OK, return to recv()
                pass
            except:
                self.shutdown_flag.set()
                self.callback_queue.put(False)


    def parse_message(self, msg_id, msg_buf):
        ''' parse the HTL message '''

        # is there an affirmative response from AddCollector?
        message_name = htl_helper.get_message_name(msg_id)
        print ("Received message: ", message_name)

        try:
            message_object = htl_helper.get_message_class(msg_id)()
            message_object.ParseFromString(msg_buf)

            if msg_id == 2:
                unit_status_message_json = MessageToJson(message_object)
                # commented out logging due to high periodic frequency of receiving this message type
                # print(unit_status_message_json)

            if msg_id == 5:
                # error with collector
                error_message = "HTL Msg - Error with Collector ID: " + str(message_object.collector_id) + " Error Message: " + message_object.error_string
                print("Error - ", error_message)

                # send Tews Results
                message = MicroMessage.TewsMessage()
                # Send error result
                message.configure_ddc_result.signal_id = 0
                message.configure_ddc_result.action = MicroMessage.ConfigureDdcResult.STOP
                message.configure_ddc_result.result = MicroMessage.ConfigureDdcResult.FAILED
                message.configure_ddc_result.reason = message_object.error_string
                self.send_socket.sendto(message.SerializeToString(), (self.config['tews_proto_dest_ip'], self.config['tews_proto_dest_port']))

                try:
                    self.parent.running_collector_list.remove(message_object.collector_id)

                    if len(self.parent.running_collector_list) > 0:
                        self.connection_status = "Connected. Collectors Running: " + str(self.parent.running_collector_list)
                    else:
                        self.connection_status = "Connected. No Collectors Running."

                except:
                    pass

            elif msg_id == 4:
                # delete collector
                error_message = "HTL Msg - Deleting Collector ID: " + str(message_object.collector_id)
                print("Alert - ", error_message)

                try:
                    self.parent.running_collector_list.remove(message_object.collector_id)

                    if len(self.parent.running_collector_list) > 0:
                        self.setConnectionStatus = "Connected. Collectors Running: " + str(self.parent.running_collector_list)
                    else:
                        self.setConnectionStatus = "Connected. No Collectors Running."

                except:
                    pass


            if msg_id == 7:
                # error with tuner
                error_message = "HTL Msg - Error with Tuner ID: " + str(message_object.emitter_id) + " Error Message: " + message_object.error_string
                print("Error - ", error_message)

                # send Tews Results
                message = MicroMessage.TewsMessage()

                # Send error result
                message.sdr_tune_result.results = MicroMessage.ConfigureDucResult.FAILED
                message.sdr_tune_result.reason = message_object.error_string
                self.send_socket.sendto(message.SerializeToString(), (self.config['tews_proto_dest_ip'], self.config['tews_proto_dest_port']))



            if msg_id == 11:
                # error with emitter
                error_message = "HTL Msg - Error with Emitter ID: " + str(message_object.emitter_id) + " Error Message: " + message_object.error_string
                print("Error - ", error_message)

                # send Tews Results
                message = MicroMessage.TewsMessage()

                # Send error result
                message.configure_duc_result.action = MicroMessage.ConfigureDucResult.STOP
                message.configure_duc_result.result = MicroMessage.ConfigureDucResult.FAILED
                message.configure_duc_result.reason = message_object.error_string
                message.configure_duc_result.transmit_id = message_object.emitter_id
                self.send_socket.sendto(message.SerializeToString(), (self.config['tews_proto_dest_ip'], self.config['tews_proto_dest_port']))

                try:
                    self.parent.running_emitter_list.remove(message_object.emitter_id)

                    if len(self.parent.running_emitter_list) > 0:
                        self.connection_status = "Connected. Emitters Running: " + str(self.parent.running_emitter_list)
                    else:
                        self.connection_status = "Connected. No Emitters Running."

                except:
                    pass

            elif msg_id == 10:
                # delete emitter
                error_message = "HTL Msg - Deleting Emitter ID: " + str(message_object.emitter_id)
                print("Alert - ", error_message)

                try:
                    self.parent.running_emitter_list.remove(message_object.emitter_id)

                    if len(self.parent.running_emitter_list) > 0:
                        self.setConnectionStatus = "Connected. Emitters Running: " + str(self.parent.running_emitter_list)
                    else:
                        self.setConnectionStatus = "Connected. No Emitters Running."

                except:
                    pass

        except Exception as e:
            print( "Exception in parse_message: " + str(e))
            return None

class DataSharingObject():
    ''' a simple object for sharing data on sychronized queues '''

    def __init__(self, channel_id, running_time_in_seconds, stream_id):
        self.channel_id = channel_id
        self.running_time_in_seconds = running_time_in_seconds
        self.stream_id = stream_id

class StartTCPServer(threading.Thread):
    ''' thread for setting up TCP Server that will allow for sending of RAW IQ packets to HTL
        HTL will be client that connects to this server

    '''

    def __init__(self, tcp_ip, tcp_port, config, shared_queue, parent):
        super(StartTCPServer, self).__init__()

        self._shutdown_server_event = threading.Event()

        self._shutdown_client_event = threading.Event()
        self._shutdown_client_event.set()

        self.connection_socket = None

        self.client_socket_list = []

        self.tcp_ip = tcp_ip

        self.tcp_port = tcp_port

        self.parent = parent

        self.config = config

        self.shared_queue = shared_queue

        self.active_thread = None

        self.iq_sender = None

        self.tx_timeout = config['tx_timeout']


    def start_server(self):
        self._shutdown_server_event.clear()

    def stop_server(self):
        self._shutdown_server_event.set()

    # shutdown_server forces the socket closed, throwing an exception and exiting thread
    def shutdown_server(self):
        self.stop_server()
        # have to actively kill the socket or the thread waits on Accept()
        # and doesn't catch the _shutdown_server_event until the next connection
        if self.connection_socket is not None:
            self.connection_socket.shutdown(socket.SHUT_RDWR)
            self.connection_socket.close()

    def stopped_server(self):
        return self._shutdown_server_event.is_set()

    def start_client(self):
        self._shutdown_client_event.clear()

    def stop_client(self):
        print("stop_client")
        self._shutdown_client_event.set()
        # reset so next ConfigureDucRequest will cause a new thread
        self.active_thread = None
        self.iq_sender = None

    def stopped_client(self):
        return self._shutdown_client_event.is_set()

    def run(self):

        self.connection_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connection_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        try:
            self.connection_socket.bind(('', self.tcp_port))
            self.connection_socket.listen(5) # max backlog of connections

            print( '[IQ Transmit Server] - Listening on {}:{}'.format(self.tcp_ip, self.tcp_port) )

        except Exception as e:
            print("[IQ Transmit Server] - TCP Bind Error:" + str(e))
            self.stop_server()


        while not self.stopped_server():
            try:
                client_sock, address = self.connection_socket.accept()
                print ('[IQ Transmit Server] - Accepted connection from {}:{}'.format(address[0], address[1]) )
                self.client_socket_list.append(client_sock)

                try:
                    # Only need one thread.  It will reference client_socket_list.
                    if self.active_thread is None:
                        self.active_thread = Thread(target=self.client_thread, args=(client_sock, self.tcp_ip, self.tcp_port, self.shared_queue))
                        self.active_thread.start()
                    else:
                        if self.iq_sender is not None:
                            self.iq_sender.addSock(client_sock)
                        else:
                            print("[IQ Transmit Server] - self.iq_sender is None.  Can't add new socket")

                except Exception as e:
                    print("[IQ Transmit Server] - Error in TCP Client Handling Thread")
                    print(e)


            except Exception as e:
                self.stop_server()
                print("[IQ Transmit Server] - Exception ")
                print(e)


        # terminate all client sockets
        print('[IQ Transmit Server] - Closing TCP Server for IQ that was listening on {}:{}'.format(self.tcp_ip, self.tcp_port))
        try:
            for client_sock in self.client_socket_list:
                client_sock.close()
            self.client_socket_list.clear()
            # terminate listening socket
            self.connection_socket.close()
        except Exception as e:
            print("[IQ Transmit Server] - Exception at thread exit")
            print(e)

    def teardown_all_ducs(self):

        # safely delete all DUCs
        for i in range(self.config['max_num_emitters']):
            self.parent.delete_emitter(emitter_id=i)


    def client_thread(self, client_connection_socket, ip, port, shared_queue):
        '''  
            will read from tews backplane and send IQ to HTL interface for transmission
        '''

        running = True

        requested_channel_id = None

        print("[IQ Transmit Server] - waiting for modularistart")
        # wait for modulari
        while self.stopped_client():
            time.sleep(0.1)

        try:
            while not shared_queue.empty():
                data = shared_queue.get(True, self.tx_timeout)

                if type(data) is int:
                    print(data)

                # received unknown object
                if type(data) is not DataSharingObject:
                    print("[IQ Transmit Server] - type is not DataSharingObject")
                    continue

                # # data is not request
                # if len(data) < 3:
                #     print("len of data is less than 3")
                #     continue

                requested_channel_id = data.channel_id
                requested_run_time_in_seconds = data.running_time_in_seconds
                requested_stream_id = data.stream_id

                # clear queue to prevent overflow
                with shared_queue.mutex:
                    shared_queue.queue.clear()

                print("[IQ Transmit Server] - post mutex")
                # create Attack instance for sending IQ
                try:

                    self.iq_sender = Attack(channel=requested_channel_id, socket=client_connection_socket, format=0, stream_id=requested_channel_id)
                    # add any additional sockets
                    for sock in self.client_socket_list[1:]:
                        self.iq_sender.addSock(sock)
                    print("[IQ Transmit Server] - Creating Attack Instance: Channel ID:" + str(requested_channel_id))
                except Exception as e:
                    print("[IQ Transmit Server] - Error Creating Attack Instance. Stopping sending of IQ Thread")
                    print(e)
                    running = False

                    # safely delete all DUCs
                    self.teardown_all_ducs()

                    for sock in self.client_socket_list:
                        sock.close()
                    self.client_socket_list.clear()
                    return


                start_time = time.time()
                end_time = start_time + requested_run_time_in_seconds

                print("[IQ Transmit Server] - Handling Queued Request for Chan: " + str(requested_channel_id) + ", len: " + str(requested_run_time_in_seconds) + " seconds")

                running = True

                while running and not self.stopped_client():
                    try:
                        self.iq_sender.step()

                        if time.time() > end_time:
                            running = False
                    except:
                        running = False

                        # safely delete all DUCs
                        self.teardown_all_ducs()

                        for sock in self.client_socket_list:
                            sock.close()
                            print('[IQ Transmit Server] - closing socket from ' + ip )

                        self.client_socket_list.clear()
                        pass

            try:
                self.iq_sender.free()
            except Exception as e:
                queue_empty = shared_queue.empty()
                print('[IQ Transmit Server] - Could not free Attack object queue.empty() = ' + str(queue_empty) )
                print(e)

                # safely delete all DUCs
                self.teardown_all_ducs()
                self.client_socket_list.clear()

        except queue.Empty:
            print('[IQ Transmit Server] - Empty Queue for channel: ' + str(requested_channel_id))

            # safely delete all DUCs
            self.teardown_all_ducs()
            self.client_socket_list.clear()

        # End
        print('[IQ Transmit Server] - Attack Timer Expired for channel: ' + str(requested_channel_id) + ' No more transmit requests received.')

        # safely delete all DUCs
        self.teardown_all_ducs()
        self.client_socket_list.clear()




