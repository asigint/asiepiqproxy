''' 
    Data Rights Notice

        This software was produced for the U. S. Government under Basic Contract No. W15P7T-13-C-A802, and is subject to the Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation Clause 252.227-7014 (FEB 2012)    
        Copyright 2017 The MITRE Corporation. All Rights Reserved.

    .. note ::
        Classes is this module:
            :class:`giskard`

    :author: Kevin Farr - kfarr@mitre.org
    :data: 12/18/18


'''
'''

Name: ASI Proxy
Description: Proxy interface to the ASI EPIQ Stack


'''
from src.license import DATA_RIGHTS
from src.version import __version__
import threading

__author__ = "Kevin Farr - kfarr@mtire.org"
__author__ = "Michael Kumaresan - mkumaresan@mtire.org"
__author__ = "Bill Urrego - wurrego@mitre.org"
__license__ = DATA_RIGHTS
__version__ = __version__
__last_modified__ = '12/18/18'


# python module core dependencies
import socket
import json
import time
import os
import queue
import argparse
import threading

# python module external dependencies
from google.protobuf.json_format import MessageToJson

# python module internel dependencies
import src.TEWSuServiceMessaging_pb2 as MicroMessage
from src.radio_specific.tuner import Tuner
from src.logger.eventLogger import eventLogger, LOG_TYPE_CRITICAL, LOG_TYPE_ERROR, LOG_TYPE_INFO, LOG_TYPE_WARNING

# Logging TAG
TAG = '[ASI PROXY] '

''' print to console and log to file '''
def printAndLogIt( message, rcv_message, logger ):

    print(TAG, message)
    logger.write(LOG_TYPE_INFO, message)
    if rcv_message is not None:
        rcv_message_json = MessageToJson(rcv_message)
        logger.write(LOG_TYPE_INFO, rcv_message_json)


def send_sdr_tune_results(error_number, send_connection, config, reason=None):
    """
        generate sdr tune request TEWS proto results message
    """

    message = MicroMessage.TewsMessage()

    # Send error result
    message.sdr_tune_result.results = error_number
    if reason:
        message.sdr_tune_result.reason = reason
    send_connection.sendto(message.SerializeToString(), (config['tews_proto_dest_ip'], config['tews_proto_dest_port']))

    return


def load_config():
    """
        load the configuration file parameters

        :return: config dict
    """
    print(TAG, "Loading config")
    config = None
    try:
        with open('config.ini') as json_data_file:
            config = json.load(json_data_file)
    except:
        print(TAG, "ERROR - Could not load selected config.ini or JSON file format error.")

    return config



def init_tews_network(config, logger):
    """
        Initialize the network services for receiving and sending TEWS UDP messages.  It could be any client.

        :return: network service objects (rx_conn,tx_conn)
    """
    print(TAG, "Initializing Network")
    logger.write(LOG_TYPE_INFO, "Initializing Network")

    # Receiver
    rcv_connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    rcv_connection.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    try:
        rcv_connection.bind(('', config['tews_proto_rx_bind_port']))
        print(TAG, 'Receiving at UDP Port ', config['tews_proto_rx_bind_port'])
        logger.write(LOG_TYPE_INFO, 'Receiving at UDP Port ' + str(config['tews_proto_rx_bind_port']))

    except:
        print(TAG, 'ERROR - Unable to bind socket to port.')
        #logger.write(LOG_TYPE_CRITICAL, 'Unable to bind socket to port: ' + str(config['tews_proto_rx_bind_port']))
        return None, None

    # Sender
    tews_send_connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    tews_send_connection.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    return rcv_connection, tews_send_connection


def init_radio_connection(config, logger, callback_queue, tews_send_connection ):

    # Radio Connection
    radio_tuner = Tuner(None, config, logger)

    # Idle until connected to Radio
    idle_until_connected_to_radio(radio_tuner, config, tews_send_connection, callback_queue, logger)

    return radio_tuner

def idle_until_connected_to_radio(radio_tuner, config, tews_send_connection, callback_queue, logger):
    '''
        Wait forever until connected to radio.  Don't want to depend on the radio being
        booted before the proxy uservice starts.  Also used to re-connect after a radio
        is turned off, disconnected, etc.
    '''

    connected = radio_tuner.connected

    # Wait until we have a connection to the radio
    while not connected:
        connected = radio_tuner.connect_to_radio()

        if not connected:

            # log
            printAndLogIt('Failed to connect to ASI, waiting...', None, logger)

            # send SDR Not Connected to TEWS Protoplane even if not actively trying to Tune
            send_sdr_tune_results(MicroMessage.SDRTuneResult.NOT_CONNECTED, tews_send_connection, config)
            time.sleep(config['initial_connect_retry_period_seconds'])


    # send SDR Connected to TEWS Protoplane
    send_sdr_tune_results(MicroMessage.SDRTuneResult.CONNECTED, tews_send_connection, config)
    printAndLogIt('Connected to radio', None, logger)


def radio_control(config, radio_id):
    """
       primary logic for handling radio interface

       Here is where the process listens for incoming client requests, parses them, makes
       calls to the radio, and responds.  A basic example of a tune request is shown.

       radio_id parameter can be used to establish a one-to-one relationship between a proxy
       and a radio when multiple radios are present.  It can also be used to create unique
       behavior of a proxy (for example, a proxy that behaves differently for Electronic Attack
       or Electronic Surveillance based on radio_id.


    """

    # init logger
    logger = eventLogger()
    printAndLogIt('Starting up Radio control', None, logger)

    # setup queue for passing control data back to main thread
    callback_queue = queue.Queue()

    # Initialize Network
    # init_tews_network() creates 2 UDP sockets for receiving/sending Google protobuf messsages
    # from/to syste clients
    tews_rcv_connection, tews_send_connection = init_tews_network(config, logger)
    
    # Next, we set up a control channel to the radio.  This example shows a TCP/IP socket
    # connection.  See sockets_manager.py ReceivePntInfo and MonitorRadio for examples
    # using a REST API.  In the REST API case, you might modify init_radio_connection()
    # to make a basic status request and not return until it's successful.
    radio_tuner = init_radio_connection(config, logger, callback_queue, tews_send_connection)

    if tews_rcv_connection is None:
        print(TAG, "No Exiting.")
        logger.write(LOG_TYPE_INFO, "Exiting.")
        return

    # Start main loop to process remote commands
    running = True
    while (running):

        # Receive a protobuf message from a client
        data = tews_rcv_connection.recv(config['receive_buffer'])
        
        # Create an empty TewsMessage object, which is a union of all the possible TEWS protobuf messages
        rcv_message = MicroMessage.TewsMessage()
        # Use the data received from the client to populate the TewsMessage object
        rcv_message.ParseFromString(data)
        

        # Use "HasField('name_of_tews_message_field') to see if a sub-message is present
        if rcv_message.HasField("sdr_tune_request"):
            requested_radio_id = radio_id
            # get requested radio id
            if rcv_message.sdr_tune_request.HasField("radio_id"):
                requested_radio_id = rcv_message.sdr_tune_request.radio_id

            # check radio id
            if requested_radio_id == radio_id:

                # log info
                printAndLogIt("SDRTuneRequest received", rcv_message, logger)

                # get requested tuning parameters from tews protobuf message

                _tuner_id = rcv_message.sdr_tune_request.tuner_id
                _center_freq_hz = rcv_message.sdr_tune_request.center_frequency_hz

                # Tune to a frequency - radio_tuner object is responsible for creating a radio-specific message
                # and sending it to the radio.

                # add_tuner expects a list for tuner_id_list
                _tuner_id = []
                
                #Tune to desired frequency
                boolean_add_tuner_result= radio_tuner.add_tuner(tuner_id_list=_tuner_id, center_freq_hz=_center_freq_hz)

                # generate TEWS Protobuf SDR Results Message
                if boolean_add_tuner_result is True:
                    result = MicroMessage.SDRTuneResult.SUCCESS
                    print(TAG, "Tune Succesful")
                elif boolean_add_tuner_result is False:
                    result = MicroMessage.SDRTuneResult.FAILED
                    print(TAG,"Failed to tune radio")
                else:
                    result = MicroMessage.SDRTuneResult.FAILED
                    print(TAG,"Response from radio timed out, state unknown")
                # send SDR Tune Result to TEWS Protoplane
                send_sdr_tune_results(result, tews_send_connection, config)

            else:  # requested radio id does not match

                # log info
                message = "Ignoring SDRTuneRequest for Radio ID: " + str(requested_radio_id) + ", My Radio ID: " + str(radio_id)
                printAndLogIt(message, rcv_message, logger)

            # check if connected to Epiq
            if not radio_tuner.connected:
                logger.write(LOG_TYPE_ERROR, "Lost Connection to ASI.")

                radio_tuner.connected = False
                try:
                    radio_tuner.radio_connection_socket.close()
                except:
                    pass
                idle_until_connected_to_radio(radio_tuner, config, tews_send_connection, callback_queue, logger)



def main():
    """
       uService for radio proxy with several common example tasks

       1. optional arguments and setting configuration items
       2. Starting a thread to monitor the radio
       
       
    """

    # Load config
    config = load_config()

    # example of an argument parser
    parser = argparse.ArgumentParser()

    # add arguments to parse out
    parser.add_argument('--radio_id', action='store', type=int, default=8, dest='radio_id',help='Integer identifer for ASI SDR to connect with')
    
    # parse command line arguments
    cmd_line_parse_results = parser.parse_args()

    # store arguments
    config['radio_id'] = cmd_line_parse_results.radio_id

    radio_control(config, config['radio_id'])



if __name__ == '__main__':
    main()
